"""
    frnewsletter URL Configuration
"""
from django.contrib import admin
from django.urls import path, include, re_path, register_converter

from news.views import *
from .yasg import urlpatterns as doc_urls
from .path_converters import DateConverter

register_converter(DateConverter, 'date')

urlpatterns = [
    path('admin/', admin.site.urls),

    # client
    path('api/v1/client/', ClientViewSet.as_view({'get': 'list'}), name='client_list'),
    path('api/v1/client/', ClientViewSet.as_view({'post': 'create'}), name='client_create'),
    path('api/v1/client/<int:pk>/', ClientViewSet.as_view({'get': 'retrieve'}), name='client_retrieve'),
    path('api/v1/client/<int:pk>/', ClientViewSet.as_view({'post': 'update'}), name='client_post'),
    path('api/v1/client/<int:pk>/delete/', ClientViewSet.as_view({'get': 'destroy'}), name='client_destroy'),

    # newsletter
    path('api/v1/newsletter/', NewsletterViewSet.as_view({'get': 'list'}), name='newsletter_list'),
    path('api/v1/newsletter/unsent_only/', NewsletterViewSet.as_view({'get': 'unsent_only'}), name='newsletter_unsent_only'),
    path('api/v1/newsletter/<int:pk>/messages/', NewsletterViewSet.as_view({'get': 'list_messages'}), name='newsletter_msg_list'),
    path('api/v1/newsletter/<int:pk>/unsent/', NewsletterViewSet.as_view({'get': 'unsent_messages'}), name='newsletter_unsent_list'),
    path('api/v1/newsletter/', NewsletterViewSet.as_view({'post': 'create'}), name='newsletter_list'),
    path('api/v1/newsletter/<int:pk>/', NewsletterViewSet.as_view({'get': 'retrieve'}), name='newsletter_retrieve'),
    path('api/v1/newsletter/<int:pk>/', NewsletterViewSet.as_view({'post': 'update'}), name='newsletter_post'),
    path('api/v1/newsletter/<int:pk>/abort/', NewsletterViewSet.as_view({'get': 'abort'}), name='newsletter_abort'),
    path('api/v1/newsletter/<int:pk>/delete/', NewsletterViewSet.as_view({'get': 'destroy'}), name='newsletter_destroy'),
    path('api/v1/newsletter/report/<date:date>/', NewsletterViewSet.as_view({'get': 'report_daily'}), name='newsletter_report_daily'),
    path('api/v1/newsletter/report/<date:date1>/<date:date2>/', NewsletterViewSet.as_view({'get': 'report_diap'}), name='newsletter_report'),

    # message
    path('api/v1/message/<int:pk>/start/', MessageStartAPIView.as_view(), name='message_start'),
    # message
    path('api/v1/message/<int:pk>/finish/', MessageReportAPIView.as_view(), name='message_finish'),

    # djoser
    re_path(r'^auth/', include('djoser.urls.authtoken')),
    path('api/v1/auth/', include('djoser.urls')),
    path('api/v1/drf-auth/', include('rest_framework.urls')),
]

urlpatterns += doc_urls
