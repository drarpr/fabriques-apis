# Generated by Django 4.1.1 on 2022-09-22 13:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newsletter',
            name='client_filter',
            field=models.CharField(blank=True, max_length=255, verbose_name='Фильтр'),
        ),
    ]
