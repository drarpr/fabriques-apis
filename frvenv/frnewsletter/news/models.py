from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator

import pytz
import datetime
import frnewsletter.settings as settings


class Newsletter(models.Model):
    """ Уведомление / рассылка """

    datetime_added = models.DateTimeField(auto_now_add=True,
        verbose_name='Время добавления')
    datetime_started = models.DateTimeField(default=datetime.datetime.now(), 
        verbose_name='Время начала')
    datetime_stop = models.DateTimeField(default=datetime.datetime.now(), 
        verbose_name='Время окончания')

    is_active = models.BooleanField(default=False,
        verbose_name='Рассылка активна')

    # разрешённые для отправки часы по времени клиетна
    active_hour_start = models.PositiveIntegerField(null=True, blank=True,
        validators=[MinValueValidator(0), MaxValueValidator(23)],
        verbose_name='Разрешённые часы с', default=0)
    active_hour_finish = models.PositiveIntegerField(null=True, blank=True,
        validators=[MinValueValidator(0), MaxValueValidator(23)],
        verbose_name='Разрешённые часы по', default=23)

    message = models.CharField(max_length=255, null=False, unique=False, 
        verbose_name='Сообщение')

    client_tag_filter = models.CharField(max_length=255, blank=True, null=False, 
        unique=False, verbose_name='Фильтр: тэг')
    client_operator_filter = models.CharField(max_length=255, blank=True, 
        null=False, unique=False, verbose_name='Фильтр: оператор')

    def __str__(self):
        return f'{self.datetime_added} / {self.message}'
    
    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'
        ordering = ['-pk']


class Client(models.Model):
    """ Клиент """

    phone = PhoneNumberField(blank=True, unique=True,
        verbose_name='Номер телефона')
    operator = models.CharField(max_length=3, default='', blank=True, null=True,
        verbose_name='Оператор')
    tag = models.CharField(max_length=255, default='', blank=True, null=True,
        verbose_name='Тэг')
    
    ALL_TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))
    timezone = models.CharField(max_length=32, choices=ALL_TIMEZONES, 
        default='UTC', verbose_name='Часовой пояс')

    def __str__(self):
        return f'{self.phone}'

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'
        ordering = ['phone']


class Message(models.Model):
    """ Сообщение """

    datetime_added = models.DateTimeField(auto_now_add=True, 
        verbose_name='Дата добавления')
    datetime_started = models.DateTimeField(null=True, blank=True,
        verbose_name='Дата начала отправки')
    datetime_finished = models.DateTimeField(null=True, blank=True, 
        verbose_name='Дата отправки')

    MSG_NEW = 0
    MSG_SENT = 1
    MSG_ERROR = 2
    message_statuses = [
        (0, 'New'),
        (1, 'Sent'),
        (2, 'Error'),
    ]
    status = models.IntegerField(choices=message_statuses, default=MSG_NEW,
        verbose_name='Статус')
    newsletter = models.ForeignKey('Newsletter', on_delete=models.CASCADE,
        related_name='messages', related_query_name='messages',
        verbose_name='Рассылка')
    client = models.ForeignKey('Client', on_delete=models.CASCADE, 
        related_name='messages', related_query_name='messages',
        verbose_name='Клиент')

    def __str__(self):
        return f'{self.newsletter_id} / {self.newsletter.message} / {self.client}'

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
        ordering = ['newsletter', '-pk']
