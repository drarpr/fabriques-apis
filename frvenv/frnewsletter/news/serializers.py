from rest_framework import serializers
from .models import *


class ClientSerializer(serializers.ModelSerializer):
    """ Клиент """

    class Meta:
        model = Client
        fields = "__all__"


class MessageSerializer(serializers.ModelSerializer):
    """ Сообщение """

    client = ClientSerializer()

    class Meta:
        model = Message
        fields = "__all__"

class MessageFinishSerializer(serializers.ModelSerializer):
    """ Сообщение """

    class Meta:
        model = Message
        fields = ['status']

class NewsletterReportSerializer(serializers.ModelSerializer):
    """ Рассылка """

    message_stats = serializers.SerializerMethodField()
    
    def get_message_stats(self, instance:Newsletter):
        """ получение статистики по сообщениям рассылки """
        
        dt1 = self.context.get('dt1')
        dt2 = self.context.get('dt2')
        
        grouped = {'sent':{}, 'total': {}}
        
        for val, txt in Message.message_statuses:
            grouped['sent'][val] = instance.messages.filter(status=val,
                    datetime_finished__gte=dt1, datetime_finished__lte=dt2
                ).count()

            grouped['total'][val] = instance.messages.filter(status=val).count()
        
        return grouped

    class Meta:
        model = Newsletter
        fields = "__all__"
class NewsletterSerializer(serializers.ModelSerializer):
    """ Рассылка """

    message_stats = serializers.SerializerMethodField()
    
    def get_message_stats(self, instance:Newsletter):
        """ получение статистики по сообщениям рассылки """
        grouped = {}
        
        for val, txt in Message.message_statuses:
            grouped[val] = instance.messages.filter(status=val).count()
        
        return grouped

    class Meta:
        model = Newsletter
        fields = "__all__"

# API для обработки активных рассылок и отправки сообщений клиентам
