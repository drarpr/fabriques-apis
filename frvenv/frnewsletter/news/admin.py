from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html

from .models import Newsletter, Client, Message
from .forms import *

class NewsletterAdmin(admin.ModelAdmin):
    form = NewsletterForm
    list_display = ['message', 'datetime_started', 'datetime_stop', 'client_tag_filter', 'client_operator_filter']
    search_fields = ['message', 'client_tag_filter', 'client_operator_filter']
    readonly_fields = ['datetime_added']
    list_filter = ['datetime_started', 'datetime_stop', 'client_tag_filter', 'client_operator_filter']

    save_on_top = True


class MessageAdmin(admin.ModelAdmin):
    list_display = ['pk', 'client_phone', 'status', 'datetime_added', 'datetime_started', 'datetime_finished', 'link_to_Newsletter']
    def link_to_Newsletter(self, obj):
        link_url = reverse('admin:news_newsletter_change', args=[obj.newsletter.id])
        return format_html(f'<a href="{link_url}">{obj.newsletter}</a>')
    link_to_Newsletter.allow_tags=True
    search_fields = ['client__phone', 'newsletter__message']
    list_display_links = ['pk']
    readonly_fields = ['datetime_added', 'datetime_started', 'datetime_finished']
    list_editable = ['status']
    list_filter = ['newsletter', 'datetime_started', 'datetime_finished']
    
    def client_phone(self, object):
        """ удаление + в начале номера телефона в админке """
        
        return str(object.client.phone)[1:]

    client_phone.short_description = 'Номер телефона'
    link_to_Newsletter.short_description = 'Ссылка на рассылку'
    
    save_on_top = True
    
        
class ClientAdmin(admin.ModelAdmin):
    list_display = ['phone_safe', 'operator', 'tag', 'timezone']
    search_fields = ['phone', 'operator', 'tag', 'timezone']
    list_editable = ['operator', 'tag', 'timezone']
    list_filter = ['operator', 'tag', 'timezone']
    
    def phone_safe(self, object):
        """ удаление + в начале номера телефона в админке """

        return str(object.phone)[1:]
    
    phone_safe.short_description = 'Номер телефона'

    save_on_top = True


admin.site.register(Client, ClientAdmin)
admin.site.register(Message, MessageAdmin)
admin.site.register(Newsletter, NewsletterAdmin)
