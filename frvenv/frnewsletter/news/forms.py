from django import forms
from .models import *

class NewsletterForm(forms.ModelForm):
    def clean(self):
        """ protect active/in-progress Newsletters from being changed """
        
        cleaned_data = super().clean()
        
        instance:Newsletter = self.instance

        hour_st = cleaned_data.get('active_hour_start')
        hour_fi = cleaned_data.get('active_hour_finish')
        
        # check the st/fi order
        if hour_fi and hour_st and hour_fi < hour_st:
            raise ValidationError('Значение "Разрешённые часы с" должно быть менее или равно значению "Разрешённые часы по"')

        # check while editing        
        if instance:
            # deny modifying active / in-progress Newsletter
            now = datetime.datetime.now(tz=pytz.timezone(settings.TIME_ZONE))
            if cleaned_data['is_active'] and instance.is_active and instance.datetime_started <= now < instance.datetime_stop:
                #if self.is_active and self.datetime_started <= now < self.datetime_stop:
                raise ValidationError('Запрещено редактировать активные и отправляемые рассылки!')
        
    class Meta:
        model = Newsletter
        fields = '__all__'