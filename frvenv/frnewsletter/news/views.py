from django.shortcuts import render
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models import Exists, OuterRef, Q
   
from rest_framework import viewsets, status, permissions, generics, mixins
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.renderers import JSONRenderer, BrowsableAPIRenderer
from rest_framework.response import Response

from .serializers import *
from .models import *
import frnewsletter.settings as settings

class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    renderer_classes = [JSONRenderer, BrowsableAPIRenderer]
    permission_classes = [permissions.IsAuthenticated]

@receiver(post_save, sender=Newsletter, dispatch_uid="update_newsletter")
def init_newsletter_messages(sender, instance, **kwargs):
    """ создание списка сообщений для рассылки для неактивной рассылки """
    
    nlinstance = instance
    
    # not going to update is_active / in_process newsletter
    now = datetime.datetime.now(tz=pytz.timezone(settings.TIME_ZONE))
    if nlinstance.is_active and nlinstance.datetime_started <= now < nlinstance.datetime_stop:
        return
    
    # delete all messages for this instance
    nlinstance.messages.all().delete()
    
    filter = {}
    if nlinstance.client_tag_filter:
        filter['tag'] = nlinstance.client_tag_filter
    if nlinstance.client_operator_filter:
        filter['operator'] = nlinstance.client_operator_filter
        
    clients = Client.objects.filter(**filter).all()
    
    for cl in clients:
        m = Message()
        m.status = 0
        m.newsletter = nlinstance
        m.client = cl
        m.save()


class NewsletterViewSet(viewsets.GenericViewSet, generics.ListCreateAPIView,
    generics.RetrieveUpdateAPIView):
    """ Рассылка уведомлений """

    queryset = Newsletter.objects.all()
    serializer_class = NewsletterSerializer
    renderer_classes = [JSONRenderer, BrowsableAPIRenderer]
    permission_classes = [permissions.IsAuthenticated]

    def abort(self, request, pk):
        """ срочная отмена отправки рассылки """

        # update Newsletter not calling a post_save signal        
        instance = self.get_object()
        if not instance.is_active:
            return Response({'message': 'already aborted'}, status=status.HTTP_400_BAD_REQUEST)            

        Newsletter.objects.filter(pk=pk).update(is_active=False)
        return Response({'newsletter': self.get_serializer(instance, many=False).data})

    def update(self, request, pk):
        """ переопределение update метода """
        
        instance:Newsletter = self.get_object()
        now = datetime.datetime.now(tz=pytz.timezone(settings.TIME_ZONE))
        
        # update only planned newsletters, not active/finished
        if not instance.is_active or now < instance.datetime_started:
            serializer = self.get_serializer(instance=instance, 
                data=request.data)
            if serializer.is_valid(raise_exception=True):

                serializer.save()
                return Response({'newsletter': serializer.data})
            else:
                return Response(serializer.errors,
                    status=status.HTTP_400_BAD_REQUEST)        
        else:
            return Response(data={'message': 'Can not update finished/active '
                'newsletters!'}, status=status.HTTP_400_BAD_REQUEST)        

    def unsent_only(self, request):
        """ выборка только рассылок с неотправленными сообщениями """
        
        queryset = self.queryset.filter(
            Exists(
                Message.objects.filter(newsletter=OuterRef('pk'), status=0)
            ),
            is_active=True
        )
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
        
    @action(detail=True, methods=['get'])
    def list_messages(self, request, pk):
        """ просмотр детальной статистики по сообщениям указанной рассылки """

        messages = self.get_object().messages.all()
        return Response(data={
                'messages': MessageSerializer(messages, many=True).data
            }, status=status.HTTP_200_OK)

    @action(detail=True, methods=['get'])
    def unsent_messages(self, request, pk):
        """ список ещё не отправленных сообщений указанной рассылки """

        newsletter = self.get_object()
        
        # don't feed gremlins after midnight! (no messages after datetime_stop)
        now = datetime.datetime.now(tz=pytz.timezone(settings.TIME_ZONE))
        if newsletter.datetime_stop < now:
            return Response(data={'messages': []}, status=status.HTTP_200_OK)

        messages = newsletter.messages.filter(status=0).all()
        return Response(data={
                'messages': MessageSerializer(messages, many=True).data
                }, status=status.HTTP_200_OK)

    @action(detail=False, methods=['post'])
    def create(self, request):
        """ создание рассылки """

        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            
            serializer.save()
            
            return Response({'newsletter': serializer.data})
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)        

    def report_daily(self, request, date):
        """ отчёт за день по рассылкам """
        
        return self.report_diap(request, date1=date, date2=date)

    def report_diap(self, request, date1, date2):
        """ отчёт за произвольный период по рассылкам """
        
        #dt1 = datetime.datetime.now(tz=pytz.timezone(settings.TIME_ZONE))
        
        print(repr(date1), date1)
        
        dt1 = datetime.datetime.combine(date1, datetime.time(0,0,0))
        dt2 = datetime.datetime.combine(date2, datetime.time(23,59,59))
        
        dt1 = dt1.astimezone(tz=pytz.timezone(settings.TIME_ZONE))
        dt2 = dt2.astimezone(tz=pytz.timezone(settings.TIME_ZONE))
        
        queryset = self.queryset.filter(messages__datetime_finished__gte=dt1, messages__datetime_finished__lte=dt2).distinct()
        serializer = NewsletterReportSerializer(queryset, many=True, context={'dt1': dt1, 'dt2': dt2})
        
        return Response(data={'result': serializer.data}, status=status.HTTP_200_OK)

class MessageStartAPIView(mixins.UpdateModelMixin, generics.GenericAPIView):
    """ запуск отправки сообещния """

    queryset = Message.objects.all()
    serializer_class = MessageFinishSerializer
    permission_classes = (permissions.IsAuthenticated, )

    def get(self, request, *args, **kwargs):
        """ отчёт о завершении задачи """

        pk = kwargs.get('pk')
        if not pk:
            return Response(data={'result': 'fail'}, status=status.HTTP_400_BAD_REQUEST)

        try:
            message:Message = Message.objects.get(pk=pk)
        except:
            return Response(data={'result': 'fail'}, status=status.HTTP_404_NOT_FOUND)

        # check current message status first
        if message.status != 0:
            return Response(data={
                    'result': 'fail', 
                    'message': 'Message is not in state 0'
                },
                status=status.HTTP_400_BAD_REQUEST)

        now = datetime.datetime.now(tz=pytz.timezone(settings.TIME_ZONE))
        now_clean = datetime.datetime.now()

        # check if sending to client is permitted
        hour_st = message.newsletter.active_hour_start
        hour_fi = message.newsletter.active_hour_finish
        if hour_fi and hour_st:
            tz1 = pytz.timezone(settings.TIME_ZONE)
            tz2 = pytz.timezone(message.client.timezone)
            tdelta = (tz1.localize(now_clean) - tz2.localize(now_clean).astimezone(tz1))
            diff_hours = (-1 if tdelta.days < 0 else 0) * 24 + tdelta.seconds/3600
            
            #print('\t','our tz', settings.TIME_ZONE)
            #print('\t','cli tz', message.client.timezone)
            #print('\t','delta', tdelta)
            #print('\t', hour_st, '<=', now_clean.hour, '+', diff_hours, '<=', hour_fi)
            
            if not (hour_st <= now_clean.hour + diff_hours <= hour_fi):
                return Response(data={
                        'result': 'fail', 
                        'message': f'its forbidden to send this message right now!'
                    },
                    status=status.HTTP_400_BAD_REQUEST)
                

        if not message.datetime_started:
            message.datetime_started = now
            message.save()

        return Response({
                'result': 'success',
                'message': self.get_serializer(message, many=False).data
            }, status=status.HTTP_200_OK)

class MessageReportAPIView(mixins.UpdateModelMixin, generics.GenericAPIView):
    """ финальное обновление статуса сообщения """

    queryset = Message.objects.all()
    serializer_class = MessageFinishSerializer
    permission_classes = (permissions.IsAuthenticated, )

    def post(self, request, *args, **kwargs):
        """ отчёт о завершении задачи """

        pk = kwargs.get('pk')
        if not pk:
            return Response(data={'result': 'fail'}, 
                status=status.HTTP_400_BAD_REQUEST)

        try:
            message:Message = Message.objects.get(pk=pk)
        except:
            return Response(data={'result': 'fail'}, 
                status=status.HTTP_404_NOT_FOUND)

        # check current message status first
        if message.status != 0:
            return Response(data={'result': 'fail', 'message': 'Message is not in state 0'}, status=status.HTTP_400_BAD_REQUEST)

        try:
            serializer = self.get_serializer(data=request.data, instance=message, partial=True)
            serializer.is_valid(raise_exception=True)
            instance:Message = serializer.save()
            instance.datetime_finished = datetime.datetime.now(tz=pytz.timezone(settings.TIME_ZONE))
            instance.save()
        except ValidationError as e:
            print(e.detail)
            return Response(data={'result': 'fail', 'message': 'Serialization failed'}, status=status.HTTP_400_BAD_REQUEST)

        return Response({'result': 'success', 'message': serializer.data}, status=status.HTTP_200_OK)
