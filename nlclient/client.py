""" 
    Fabrique Newsletter Client
"""

import os
import sys
import logging
from shutil import copyfile
from configparser import ConfigParser

from utils import *
from remote_api import FabNewsletterAPI
from pathlib import Path

###################################################################

# allow only one instance of this script running via lock files
from tendo import singleton
try:
    me = singleton.SingleInstance()
except singleton.SingleInstanceException:
    sys.exit(0)

BASE_DIR = Path(__file__).resolve().parent.parent
LOGS_DIR = BASE_DIR / 'nlclient_logs'
CONFIG_DIR = BASE_DIR / 'nlclient_config'
FORMAT = '%(asctime)s %(levelname)7s: %(message)s'
logging.basicConfig(filename= LOGS_DIR / 'client.log', filemode='a', format=FORMAT,
    datefmt='%Y-%m-%d %H:%M:%S', level=logging.INFO)
#logging.root.setLevel(logging.INFO)

# configuration part
if not os.path.exists(CONFIG_DIR / 'config.ini'):
    logging.error('file "config.ini" was not found! Check if "config.ini.example" exists, you may use it.')
    sys.exit()

# load current options
config:ConfigParser = ConfigParser()
config.read(CONFIG_DIR / 'config.ini')
server = get_option(config, 'main', 'server')
token = get_option(config, 'auth', 'token', '')
jwt_token = get_option(config, 'auth', 'jwt_token', '')

api = FabNewsletterAPI(server, token)

# authorize, get token
#logging.info('Checking user authorization...')
if token:
    auth_result, last_code = api.check_auth()
    if last_code == 0:
        logging.error(f'could not connect to the server: {api.reason}')
        sys.exit()

if not token or not auth_result:

    logging.info('logging in using username and password from "config.ini"...')
    username = get_option(config, 'auth', 'username')
    password = get_option(config, 'auth', 'password')
    if not api.authorize(username, password):
        logging.error('could not authorize with specified username and password!')
        sys.exit()

    logging.info('Saving obtained token to "config.ini"...')
    config.set('auth', 'token', api.token)
    with open(CONFIG_DIR / 'config.ini', 'w') as configfile:
        config.write(configfile)

# get newsletter list
newsletters = api.newsletter_list()
if not newsletters:
    sys.exit()

for nl in newsletters:

    # check if any newsletter has unsent messages    
    stats = nl.get('message_stats')
    if stats.get('0') > 0:
    
        newsletter_id = nl.get('id')
        message_text = nl.get('message')
        
        # get unsent messages for this newsletter
        unsent = api.unsent_messages(newsletter_id)
        if not unsent:
            continue

        # send each message
        for msg in unsent.get('messages'):
            message_id = msg.get('id')
            client_number = msg.get('client').get('phone')[1:]

            logging.info(f'Newsletter #{newsletter_id}: message #{message_id}: "{message_text}" => {client_number}')
            
            # report to our server
            if not api.message_start(message_id):
                continue

            # send a message to remote API            
            msg_result = api.send_to_fbrq_cloud(jwt_token, message_id, message_text, client_number)
            # mock fbrq_cloud answer
            #msg_result = True

            # report to our server on success
            if msg_result:
                logging.info(f'message #{message_id}: sent successfully')
                api.message_success(message_id, 1 if msg_result else 0)
            else:
                logging.info(f'message #{message_id}: wasn\'t sent, will try again later.')
                
logging.info("Done")
