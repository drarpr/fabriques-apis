""" Utils for drarpr project client """

import smtplib, ssl
from email.mime.text import MIMEText
import sys
import logging
from configparser import *

def send_mail(smtp_server, port, sender_email, password, receiver_email, message='Test message'):

    message = MIMEText(message.encode('utf8'), _charset='utf-8')
    message['Subject'] = 'Notification service'


    context = ssl.create_default_context()
    with smtplib.SMTP(smtp_server, port) as server:
        #server.set_debuglevel(1)
        #server.ehlo()  # Can be omitted
        server.starttls(context=context)
        #server.ehlo()  # Can be omitted
        server.login(sender_email, password)
        server.sendmail(sender_email, receiver_email, message.as_string())


def send_report(smtp_server, port, sender_email, password, receiver_email, msg):
    send_mail(smtp_server, port, sender_email, password, receiver_email, msg)
    
def send_empty_report(smtp_server, port, sender_email, password, receiver_email):
    send_report(smtp_server, port, sender_email, password, receiver_email, 'Оповещений за сутки не отправлялось.')

def get_option(config:ConfigParser, section:str = '', name:str = '', default:str = None):
    """ DRY: option getter """

    try:
        val = config.get(section, name)
        return val
    except NoSectionError:
        if not default is None:
            logging.warning(f'no section "{section}" found! Using default value for "{name}" instead: "{default}"')
            return default
        else:
            logging.error(f'no section "{section}" found!')
            sys.exit()
    except NoOptionError:
        if not default is None:
            logging.warning(f'no parameter "{section}"=>"{name}" found! Using default value for "{name}" instead: "{default}"')
            return default
        else:
            logging.error(f'check "{section}"=>"{name}" settings in mailer.ini!')
            sys.exit()
    except Exception:
        logging.error(f'got unknown error. Check "{section}"=>"{name}" settings in mailer.ini!')
        sys.exit()        

    return None
