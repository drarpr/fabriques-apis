import json
import requests
from datetime import date, timedelta
from copy import copy

class FabNewsletterAPI():
    """ API for Fabrique Newsletter Project """

    def __init__(self, server:str = '', token:str = ''):
        self.server = server
        self.token = token
        self.reason = ''

        self.headers = {}

    def newsletter_list(self):
        """ get the list of newsletters with unsent msgs via API """

        data = self.get(f'{self.server}api/v1/newsletter/unsent_only/')
        if self.last_code == 200:
            json_data:dict = data.json()
            return json_data
        else:
            self.newsletter_id = None
            return None

    def newsletter_report_yesterday(self):
        """ get the report via API """
 
        yesterday = date.today()  - timedelta(days=1)
 
        data = self.get(f'{self.server}api/v1/newsletter/report/{yesterday}/')
        if self.last_code == 200:
            json_data:dict = data.json()
            return json_data
            return None   

    def unsent_messages(self, id):
        """ get the list of newsletters via API """

        data = self.get(f'{self.server}api/v1/newsletter/{id}/unsent')
        if self.last_code == 200:
            json_data:dict = data.json()
            return json_data
        return None

    def send_to_fbrq_cloud(self, jwt_token, message_id, message_text, client_number):
        """ send the message to fbrq cloud """
        
        data={
            'id': message_id,
            'phone': client_number,
            'text': message_text
        }
        
        headers = {
            'Authorization': f'Bearer {jwt_token}'
        }
        
        resp = self.post(url=f'https://probe.fbrq.cloud/v1/send/{message_id}', data=data, 
            headers=headers)
        if self.last_code == 200:
            resp = resp.json()
            if resp.get('code') == 0 and resp.get('message') == 'OK':
                return True

        return False

    def message_start(self, message_id):
        """ report about the start of message sending """

        resp = self.get(f'{self.server}api/v1/message/{message_id}/start/')

        if self.last_code == 200:
            return True
        return False

    def message_success(self, message_id, new_status):
        """ report a success """

        data = {'status': new_status}

        resp = self.post(f'{self.server}api/v1/message/{message_id}/finish/',
            data=data, headers=self.headers)

        if self.last_code == 200:
            return True
        return False

    def check_auth(self):
        """ checks token """
        
        if not self.token:
            return False, 401
        
        # we have a token, check it then
        self.headers['Authorization'] = 'Token ' + self.token
        data = self.get(f'{self.server}api/v1/auth/users/me/')

        if self.last_code == 200:
            json_data:dict = data.json()
            # check if username is in the response
            if json_data.get('username', ''):
                return True, self.last_code
            return False, self.last_code

        # token is outdated        
        if self.last_code == 401:
            # need to auth, token is outdated
            self.reason = 'Token is outdated, going to log in using credentials.'
            self.token = ''
            del self.headers['Authorization']
            return False, self.last_code

        self.reason = f'FAILED, response status code is {self.last_code}'
        return False, self.last_code

    def authorize(self, username:str = '', password:str = ''):
        """ auth on server and return a token """

        auth_data = self.post(url=f'{self.server}auth/token/login/', 
            data={'username': username, 'password': password}, headers={} )

        if self.last_code == 200:
            auth_data:dict = auth_data.json()
            if token := auth_data.get('auth_token', ''):
                self.token = token
                self.headers['Authorization'] = 'Token ' + self.token
                return True

        self.reason = f'FAILED, response status code is {self.last_code}'
        return False

    def get(self, url):
        """ makes a GET request """

        self.reason = ''

        try:
            r = requests.get(url=url, headers=self.headers)
        except Exception as e:
            self.reason = f'Could not GET url: "{url}"'
            self.last_code = 0
            return None
        
        self.last_code = r.status_code

        return r

    def post(self, url, data, headers: dict = None):
        """ makes a POST request """

        self.reason = ''

        this_headers = copy(headers if headers else dict())
        this_headers['Content-type'] = 'application/json'

        try:
            r = requests.post(url=url, data=json.dumps(data), headers=this_headers)
        except Exception as e:
            self.last_code = 0
            return None
        
        self.last_code = r.status_code
        return r
