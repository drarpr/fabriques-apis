""" 
    Fabrique Newsletter Statistics Sender
"""

import os
import sys
import logging
from shutil import copyfile
from configparser import ConfigParser

from utils import *
from remote_api import FabNewsletterAPI
from pathlib import Path

###################################################################

# allow only one instance of this script running via lock files
from tendo import singleton
try:
    me = singleton.SingleInstance()
except singleton.SingleInstanceException:
    sys.exit(0)

BASE_DIR = Path(__file__).resolve().parent.parent
LOGS_DIR = BASE_DIR / 'nlclient_logs'
CONFIG_DIR = BASE_DIR / 'nlclient_config'
FORMAT = '%(asctime)s %(levelname)7s: %(message)s'
logging.basicConfig(filename= LOGS_DIR / 'mailer.log', filemode='a', format=FORMAT,
    datefmt='%Y-%m-%d %H:%M:%S', level=logging.INFO)
#logging.root.setLevel(logging.INFO)

# configuration part
if not os.path.exists(CONFIG_DIR / 'mailer.ini'):
    logging.error('file "mailer.ini" was not found! Check if "mailer.ini.example" exists, you may use it.')
    sys.exit()

# load current options
config:ConfigParser = ConfigParser()
config.read(CONFIG_DIR / 'mailer.ini')
server = get_option(config, 'main', 'server')
token = get_option(config, 'auth', 'token', '')
jwt_token = get_option(config, 'auth', 'jwt_token', '')

smtp_server = get_option(config, 'smtp', 'smtp_server')
smtp_port = get_option(config, 'smtp', 'smtp_port')
smtp_username = get_option(config, 'smtp', 'smtp_username')
smtp_password = get_option(config, 'smtp', 'smtp_password')
smtp_receiver = get_option(config, 'smtp', 'smtp_receiver')

api = FabNewsletterAPI(server, token)

# authorize, get token
#logging.info('Checking user authorization...')
if token:
    auth_result, last_code = api.check_auth()
    if last_code == 0:
        logging.error(f'could not connect to the server: {api.reason}')
        sys.exit()

if not token or not auth_result:

    logging.info('logging in using username and password from "mailer.ini"...')
    username = get_option(config, 'auth', 'username')
    password = get_option(config, 'auth', 'password')
    if not api.authorize(username, password):
        logging.error('could not authorize with specified username and password!')
        sys.exit()

    logging.info('Saving obtained token to "mailer.ini"...')
    config.set('auth', 'token', api.token)
    with open(CONFIG_DIR / 'mailer.ini', 'w') as configfile:
        config.write(configfile)

# get newsletter list
newsletters = api.newsletter_report_yesterday()
if not newsletters:
    sys.exit()

result = newsletters.get('result')
if not result:
    send_empty_report('bigmess@ya.ru')

result_text = []

sum_sent=[0,0,0]
sum_total=[0,0,0]
for nl in result:
    nl_id = nl.get('id')
    
    sent = []
    total = []
    for status in (0,1,2):
        sent.append(nl.get('message_stats').get('sent').get(str(status)))
        total.append(nl.get('message_stats').get('total').get(str(status)))
    
    sum_sent = [x+y for (x, y) in zip(sum_sent, sent)]
    sum_total = [x+y for (x, y) in zip(sum_total, total)]
    
    result_text.append(f'Рассылка оповещений #{nl_id}:')
    result_text.append(f'  отправлено: {sent[1]} / {total[1]}')
    result_text.append(f'  ошибки: {sent[2]} / {total[2]}')
    result_text.append(f'  не отправлено: {sent[0]} / {total[0]}')
    result_text.append(f'---')
    
result_text.append(f'ИТОГО ЗА СУТКИ:')
result_text.append(f'  отправлено: {sum_sent[1]} / {sum_total[1]}')
result_text.append(f'  ошибки: {sum_sent[2]} / {sum_total[2]}')
result_text.append(f'  не отправлено: {sum_sent[0]} / {sum_total[0]}')

result_text = '\n'.join(result_text)

logging.info(result_text)
send_report(smtp_server, smtp_port, smtp_username, smtp_password, smtp_receiver, result_text)